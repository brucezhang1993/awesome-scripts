#!/usr/bin/env python3
# *-- coding: utf-8 --*
import requests
from bs4 import BeautifulSoup
import sys


def main():
    if len(sys.argv) > 1:
        r = requests.get("http://www.baidu.com/baidu?wd=%s" % sys.argv[1])
        soup = BeautifulSoup(r.content, 'html5lib')
        # 匹配号码邦结果
        matches = soup.select('.op_fraudphone_net')
        if len(matches) > 0:
            match = matches[0]
            result = match.text
            print(result)
        else:
            # 匹配归属地结果
            matches = soup.select('.op_mobilephone_r span')
            if len(matches) > 1:
                match = matches[1]
                result = match.text
                print(result)
            else:
                # 匹配百度号码认证结果
                matches = soup.select('.c-gap-bottom-small a')
                if len(matches) > 0:
                    match = matches[0]
                    result = match.text
                    print(result)
                else:
                    # 匹配百度手机卫士结果
                    matches = soup.select('.op_fraudphone_word strong')
                    if len(matches) > 0:
                        match = matches[0]
                        result = match.text.strip('"')
                        print(result)
                    else:
                        print('Nothing found.')
    else:
        print('mark [NUMBER]')


if __name__ == '__main__':
    main()
